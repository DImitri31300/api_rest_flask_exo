from flask import Flask, jsonify, request
from flask_pymongo import pymongo
from flask_restful import Resource, Api 

CONNECTION_STRING = "mongodb+srv://dimitri:azeaze@cluster0-stmig.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('flask_mongodb_atlas')
user_collection = pymongo.collection.Collection(db, 'user_collection')

# creating the flask app 
app = Flask(__name__) 
# creating an API object 
api = Api(app) 
  


@app.route('/', methods = ['GET', 'POST']) 
def home(): 
    if(request.method == 'GET'): 
        data = "hello world"
        db.db.collection.insert_one({"data": data})
        return jsonify({'data': data}) 


# @app.route('/test', methods = ['GET', 'POST']) 
# def home(): 
#     if(request.method == 'GET'): 
#         data = "hello world"
#         data = jsonify({"data": data}) 
#         db.db.collection.insert_one(data)
#         return data


@app.route('/api/meteo/')
def meteo():
    dictionnaire = {
        'type': 'Prévision de température',
        'valeurs': [24, 24, 25, 26, 27, 28],
        'unite': "degrés Celcius"
    }
    return jsonify(dictionnaire)

#test to insert data to the data base
@app.route("/mongo")
def test():
    db.db.collection.insert_one({"veille": "php"})
    return "Connected to the data base!"


if __name__ == '__main__':
    app.run(port=8000)